%metodo de Punto Fijo programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end

cifras=input('ingrese el numero de cifras significativas: ');
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 

%se pide el valor de inicio para el metodo 
fprintf('en base a la grafica ingrese los siguientes datos:\n');
x0=input('ingrese el valor de x0: ');
while (x0<a || x0>b)
    x0=input('ingrese el valor de x0: ');
end

%se pide la funcion g(x) 
try
g=input('ingrese la funcion g(x)= ');
catch warning
fprintf('');
end

dg=diff(g,x);

Es=(0.5*10^(2-cifras));

if (abs(subs(dg,x0))>1) 
	fprintf('No hay convergencia\n');
    Ea=0;
    xr=0;
else
    xr=subs(g,x0);
	Ea = abs((xr-x0)/xr)*100;
end

while Ea>Es
	x0 = xr;
	xr=subs(g,x0);
	Ea = vpa(abs((xr-x0)/xr)*100);
end

fprintf('el resultado es:\n');
disp(vpa(xr));
fprintf('el error es: \n');
disp(vpa(Ea));
