%metodo de Muller programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end
cifras=input('ingrese el numero de cifras significativas: ');
Es=(0.5*10^(2-cifras));
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 

%se piden los 3 valores de inicio para el metodo 
fprintf('en base a la grafica ingrese los siguientes datos:\n');
 x1=input('ingrese el valor de x1: ');
while (x1<a || x1>b)
    x1=input('ingrese el valor de x1: ');
end

x2=input('ingrese el valor de x2: ');
while (x2<a || x2>b)
    x2=input('ingrese el valor de x2: ');
end

x3=input('ingrese el valor de x3: ');
while (x3<a || x3>b)
    x3=input('ingrese el valor de x3: ');
end

fx1=subs(f,x1);
fx2=subs(f,x2);
fx3=subs(f,x3);

h0=x2-x1;
h1=x3-x2;
s0=(fx2-fx1)/h0;
s1=(fx3-fx2)/h1;
a=(s1-s0)/(h1-h0);
b=a*h1+s1;
c=fx3;

D=(b^2-4*a*c)^(1/2);

%evaluamos la condicion
if (abs(b+D)>abs(b-D))
    xr=x3+((-2*c)/(b+D));
else
    xr=x3+((-2*c)/(b-D));
end

Ea=abs((xr-x3)/xr)*100;
while (Ea>Es)
    x1=x2;
    x2=x3;
    x3=xr;
    fx1=subs(f,x1);
    fx2=subs(f,x2);
    fx3=subs(f,x3);
    h0=x2-x1;
    h1=x3-x2;
    s0=(fx2-fx1)/h0;
    s1=(fx3-fx2)/h1;
    a=(s1-s0)/(h1-h0);
    b=a*h1+s1;
    c=fx3;
    
 
    D=vpa((b^2-4*a*c)^(1/2));
  
    
    %evaluamos la condicion
    if (abs(b+D)>abs(b-D))
        xr=vpa(x3+((-2*c)/(b+D)));
    else
        xr=vpa(x3+((-2*c)/(b-D)));
    end
    
    Ea=abs((xr-x3)/xr)*100;
end

fprintf('el resultado es:\n');
disp(vpa(xr));
fprintf('el error es: \n');
disp(vpa(Ea));


