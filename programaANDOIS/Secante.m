%metodo de la secante programa2AN
%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end

cifras=input('ingrese el numero de cifras significativas: ');
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
%se piden los 2 valores de inicio para el metodo 
fprintf('en base a la grafica ingrese los siguientes datos:\n');
 x1=input('ingrese el valor de x1: ');
while (x1<a || x1>b)
    x1=input('ingrese el valor de x1: ');
end

x2=input('ingrese el valor de x2: ');
while ((x1<a || x1>b) || x2<x1)
    x2=input('ingrese el valor de x2: ');
end
Es=(0.5*10^(2-cifras));
Ea=1000000;
i=1;
while (Ea>Es)
 
  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
  fx1=subs(f,x1);
  fx2=subs(f,x2);
  xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
  x1=x2;
  x2=xi;
  if (i==1)
    xar=xi;
    i=i+1;
  else 
    Ea=(abs((xi-xar)/xi)*100);
    xar=xi;
    i=i+1;
  end
end

fprintf('el resultado es:\n');
disp(vpa(xi));
fprintf('el error es: \n');
disp(vpa(Ea));