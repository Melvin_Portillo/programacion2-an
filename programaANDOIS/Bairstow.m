%metodo de Bairstow programa2AN
clear
clc
%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end
cifras=input('ingrese el numero de cifras significativas: ');
Es=(0.5*10^(2-cifras));
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 

A=coeffs(f,'all');
grado=numel(A)-1;
R0=input('ingrese el valor de R: ');
S0=input('ingrese el valor de S: ');

%repetitivo
while(grado>0)
    
    if(grado==1)
        raices(grado)=-A(grado+1)/A(grado);
        grado=grado-1;
    else
        if(grado==2)
            if(A(grado)^2-4*A(grado-1)*A(grado+1))<0
                raices(grado)=complex(single(-A(grado)/2*A(grado-1)),single(((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                raices(grado-1)=complex(single(-A(grado)/2*A(grado-1)),single(-((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
            else
                raices(grado)=(-A(grado)+(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                raices(grado-1)=(-A(grado)-(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
            end
            grado=grado-2;
        else

            for i=1:1:grado+1
               if i==1
                   B(1)=vpa(A(1),15);
               else
                   if i==2 
                       B(2)=vpa(A(2)+B(1)*R0,15);
                   else
                       B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                   end
               end
            end

            for i=1:1:grado
               if i==1
                   C(1)=vpa(B(1),15);
               else
                   if i==2 
                       C(2)=vpa(B(2)+C(1)*R0,15);
                   else
                       C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                   end
               end
            end

            deltaR=(((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
            deltaS=(((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
            R=R0+deltaR;
            S=S0+deltaS;

            Ear=abs(deltaR/R)*100;
            Eas=abs(deltaS/S)*100;

            while (Ear>Es || Eas>Es)
                R0=R;
                S0=S;

                for i=1:1:grado+1
                   if i==1
                       B(1)=vpa(A(1),15);
                   else
                       if i==2 
                           B(2)=vpa(A(2)+B(1)*R0,15);
                       else
                           B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                       end
                   end
                end

                for i=1:1:grado
                   if i==1
                       C(1)=vpa(B(1),15);
                   else
                       if i==2 
                           C(2)=vpa(B(2)+C(1)*R0,15);
                       else
                           C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                       end
                   end
                end

                deltaR=vpa((((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);
                deltaS=vpa((((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);

                R=R0+deltaR;
                S=S0+deltaS;

                Ear=abs(deltaR/R)*100;
                Eas=abs(deltaS/S)*100;

            end

            %verificar
            if ((R^2+4*S)<0) 
                    raices(grado)=complex(single(R/2),single(((-(R^2+4*S))^(1/2))/2));
                    raices(grado-1)=complex(single(R/2),single(-((-(R^2+4*S))^(1/2))/2));
            else
                    raices(grado) = vpa((R+(R^2+4*S)^(1/2))/2,15);
                    raices(grado-1) = vpa((R-(R^2+4*S)^(1/2))/2,15);
            end
            
            grado=grado-2;
            
            for i=1:1:grado+1
                if i==1
                   A(1)=A(1);
                else
                    if i==2 
                        A(2)=vpa(A(2)+A(1)*R);
                    else
                        A(i)=vpa(A(i)+A(i-1)*R+A(i-2)*S);
                    end
                end
            end
            
        end
    end  


end

fprintf('Las raices son:\n');
for i=1:1:numel(raices)
    disp(vpa(raices(i)));
end

