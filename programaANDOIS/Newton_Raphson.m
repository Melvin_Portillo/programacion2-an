%metodo de la Newton Raphson programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end

cifras=input('ingrese el numero de cifras significativas: ');
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
%se piden los 2 valores de inicio para el metodo 
fprintf('en base a la grafica ingrese los siguientes datos:\n');
x0=input('ingrese el valor de x0: ');

while (x0<a || x0>b)
x0=input('ingrese el valor de x0: ');
end
%verfica la convergencia
deriUno=diff(f,x);
deriDos=diff(deriUno,x);
fcero=subs(f,x0);
fderiu=subs(deriUno,x0);
fderid=subs(deriDos,x0);
convergencia=(abs((fcero*fderid)/(fderiu^2)));
calculo=(0.5*10^(2-cifras));
Es=calculo;
Ea=10000000000;
i=1;
if (convergencia<1)
  while (Ea>Es)
 
  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
  fx0=subs(f,x0);
  fx0d=subs(deriUno,x0);
  xi=(x0-(fx0)/(fx0d));
  Ea=(abs((xi-x0)/xi)*100);
  x0=xi;
  end
else 
  fprintf('no existe convergencia\n');
  xi='no definido';
  Ea='no definido';
end
fprintf('el resultado es:\n');
disp(vpa(xi));
fprintf('el error es: \n');
disp(vpa(Ea));
