%metodo de la tartaglia programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
%calculos pertinentes...xd
valoresNum=coeffs(f,'all');
w=numel(valoresNum);
grado=w-1;


s=valoresNum(1,1);
if (s==1)
  %caso tartaglia
  a=valoresNum(1,2);
  b=valoresNum(1,3);
  c=valoresNum(1,4);
else
  a=((valoresNum(1,2))/s);
  b=((valoresNum(1,3))/s);
  c=((valoresNum(1,4))/s);
end
k=(-a/3);

P=(b-((a^2)/3));
Q=(c-((a*b)/3)+((2*(a^3))/27));
E=(((4*P^3+27*Q^2)/108));

if (E<0)
  fi=acos((sqrt(27)*Q)/(2*P*sqrt((-P))));
  x1=((2*sqrt(-P/3)*cos(fi/3))+k);
	x2=((2*sqrt(-P/3)*cos((fi/3)+((2*pi)/3)))+k);
	x3=((2*sqrt(-P/3)*cos((fi/3)+((4*pi)/3)))+k);
  fprintf('Las raices son:\n');
  fprintf('x= ');
  disp(vpa(x1));
  fprintf('\nx= ');
  disp(vpa(x2));
  fprintf('\nx= ');
  disp(vpa(x3));
  
else 
  D=sqrt(E);
  if (D>0)
    A=double(-Q/2+D);
    B=double(-Q/2-D);
    
    x1=((nthroot(A,3)+nthroot(B,3))+k);
    x2a=(-1*((nthroot(A,3)+nthroot(B,3))/2)+k);
    x2b=((-sqrt(3)*((nthroot(A,3)-nthroot(B,3))/2)));
    x3a=(-1*((nthroot(A,3)+nthroot(B,3))/2)+k);
    x3b=((-sqrt(3)*((nthroot(A,3)-nthroot(B,3))/2)));
    fprintf('Las raices son:\n');
    fprintf('x= ');
    disp(vpa(x1));
    
    fprintf('\nx= ');
    disp(vpa(complex(double(x2a),double(x2b))));
    
    fprintf('\nx= ');
    disp(vpa(complex(double(x3a),double(-x3b))));
  else 
    if (Q>0)
      xx=double((Q)/2);
      xs=nthroot(vpa(xx),3);
      xd=-xs;
      x1=double(2*xd+k);
      x2=nthroot(((Q)/2),3)+k;
      x3=nthroot(((Q)/2),3)+k;
    else 
      xx=double((-Q)/2);
      xs=nthroot(vpa(xx),3);
      x1=double(2*xs+k);
      x2=nthroot(double((Q)/2),3)+k;
      x3=nthroot(double((Q)/2),3)+k;
    end
    fprintf('Las raices son:\n');
    fprintf('x= ');
    disp(vpa(x1));
    fprintf('\nx= ');
    disp(vpa(x2));
    fprintf('\nx= ');
    disp(vpa(x3));
  end
end
