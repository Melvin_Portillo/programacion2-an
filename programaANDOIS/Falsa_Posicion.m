%metodo de la falsa posicion programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end

cifras=input('ingrese el numero de cifras significativas: ');
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
%se piden los 2 valores de inicio para el metodo 
fprintf('en base a la grafica ingrese los siguientes datos:\n');
 x1=input('ingrese el valor de x1: ');
while (x1<a || x1>b)
    x1=input('ingrese el valor de x1: ');
end

x2=input('ingrese el valor de x2: ');
while ((x1<a || x1>b) || x2<x1)
    x2=input('ingrese el valor de x2: ');
end

Es=(0.5*10^(2-cifras));

fx1=subs(f,x1);
fx2=subs(f,x2);
xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
fxr=subs(f,xr);

if (fx1*fxr<0)
	x2 = xr;
else
	if (fx1*fxr>0)
		x1 = xr;
    else
        Ea=0;
    end
end
Ea=100000;

while (Ea>Es)
    xant=xr;
    fx1=subs(f,x1);
    fx2=subs(f,x2);
    xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
    fxr=subs(f,xr);

    if (fx1*fxr<0)
        x2 = xr;
    else
        if (fx1*fxr>0)
            x1 = xr;
        else
            Ea=0;
        end
    end
    
    Ea=abs((xr-xant)/xr)*100;
end

fprintf('el resultado es:\n');
disp(vpa(xr));
fprintf('el error aproximado es: \n');
disp(vpa(Ea));
