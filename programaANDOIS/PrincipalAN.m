%programa principal ......
%esto convierte a x en una variable simbolica

tipof=1;
while (tipof>=1 && tipof<=5)
    syms x
    clc
    fprintf('Bienvenido a Metodos Numericos:\n En este programa calcularemos las raices o raiz\n de una funcion segun los metodos adecuados para resolverla.\n');
    %aqui se pide la funcion para sacar las raices
    fprintf('Que tipo de funcion desea ingresar\n1.Polinomica\n2.Trigonometrica\n3.Algebraica\n4.Logaritmica\n5.Exponencial\nOtro salir\n');
    tipof=input('por favor ingrese el numero que corresponde al tipo de funcion a ingresar: ');
    
    
if (tipof==1)
    clc
    try
    f=input('ingrese la funcion polinomica Ejem(x^3+4*x^2-x+6); \nf(x)= ');
    catch warning
    fprintf('');
    end
    %aqui se pide el intervalo [a,b]
    a=input('ingrese el intervalo inferior: ');
    b=input('ingrese el intervalo superior: ');
    %esto de aqui grafica la funcion de manera simple
    ezplot(f,[a,b]);
    ylabel('Y');
    xlabel('X');
    grid on 
    hold on 
    valoresNum=coeffs(f,'all');
    w=numel(valoresNum);
    grado=w-1;
    
    if(grado==1)
        clc
        fprintf('Tipo de Metodos:\n1.Iterativos\n2.Directos\n');
          met=input('ingrese el tipo de metodo que desea: ');
          if (met==1)
            clc
            fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Newton Raphson\n4.Newton Raphson Mejorado\n5.Secante\n');
            iteMetho=input('ingrese el metodo iterativo que desea: ');
            switch (iteMetho)
              case 1
                %metodo biseccion:
                clc
                fprintf('metodo biseccion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                while (Ea>Es)
                  xr=((x1+x2)/2);
                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  fxr=subs(f,xr);
                  f1xr=(fx1*fxr);
                  if (f1xr>0)
                    x1=xr;
                  else 
                    x2=xr;
                  end
                  if (i==1)
                    xar=xr;
                    i=i+1;
                  else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(xr);
                fprintf('el error aproximado es: \n');
                disp(Ea);
                
              case 2
                %metodo falsa posicion:
                clc
                fprintf('metodo falsa posicion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end
                Ea=100000;

                while (Ea>Es)
                    xant=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                    fxr=subs(f,xr);

                    if (fx1*fxr<0)
                        x2 = xr;
                    else
                        if (fx1*fxr>0)
                            x1 = xr;
                        else
                            Ea=0;
                        end
                    end

                    Ea=abs((xr-xant)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
                
              case 3
                %Newton Raphson
                clc
                fprintf('metodo newton raphson\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');

                while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  xi=(x0-(fx0)/(fx0d));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
            
              case 4
                %Newton Raphson mejorado
                clc
                fprintf('metodo newton mejorado\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se pide valor de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while(x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                calculo=(0.5*10^(2-cifras));
                Es=calculo;
                Ea=10000000000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  fx0d2=subs(deriDos,x0);
                  xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
                
              case 5
                %secante
                clc
                fprintf('metodo secante\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end
                Es=(0.5*10^(2-cifras));
                Ea=1000000;
                i=1;
                while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
                  x1=x2;
                  x2=xi;
                  if (i==1)
                    xar=xi;
                    i=i+1;
                  else 
                    Ea=(abs((xi-xar)/xi)*100);
                    xar=xi;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
            end
                
          else
            
            raiz=-valoresNum(2)/valoresNum(1);
            fprintf("la raiz es:");
            disp(vpa(raiz));
          end
        
    elseif(grado==2)
        clc
        fprintf('Tipo de Metodos:\n1.Iterativos\n2.Directos\n');
        met=input('ingrese el tipo de metodo que desea: ');
        if (met==1)
            clc
          fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Secante\n6.Horner\n7.Muller\n');
          iteMetho=input('ingrese el metodo iterativo que desea: ');
            switch (iteMetho)
              case 1
                %metodo biseccion:
                clc
                fprintf('metodo biseccion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                while (Ea>Es)
                  xr=((x1+x2)/2);
                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  fxr=subs(f,xr);
                  f1xr=(fx1*fxr);
                  if (f1xr>0)
                    x1=xr;
                  else 
                    x2=xr;
                  end
                  if (i==1)
                    xar=xr;
                    i=i+1;
                  else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(xr);
                fprintf('el error aproximado es: \n');
                disp(Ea);
                
              case 2
                %metodo falsa posicion:
                clc
                fprintf('metodo falsa posicion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end
                Ea=100000;

                while (Ea>Es)
                    xant=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                    fxr=subs(f,xr);

                    if (fx1*fxr<0)
                        x2 = xr;
                    else
                        if (fx1*fxr>0)
                            x1 = xr;
                        else
                            Ea=0;
                        end
                    end

                    Ea=abs((xr-xant)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
              case 3
                %Punto Fijo
                clc
                fprintf('metodo punto fijo\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se pide el valor de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while (x0<a || x0>b)
                    x0=input('ingrese el valor de x0: ');
                end

                %se pide la funcion g(x) 
                try
                g=input('ingrese la funcion g(x)= ');
                catch warning
                fprintf('');
                end

                dg=diff(g,x);

                Es=(0.5*10^(2-cifras));

                if (abs(subs(dg,x0))>1) 
                    fprintf('No hay convergencia\n');
                    Ea=0;
                    xr=0;
                else
                    xr=subs(g,x0);
                    Ea = abs((xr-x0)/xr)*100;
                end

                while Ea>Es
                    x0 = xr;
                    xr=subs(g,x0);
                    Ea = vpa(abs((xr-x0)/xr)*100);
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error es: \n');
                disp(vpa(Ea));
                
              case 4
                %Newton Raphson
                clc
                fprintf('metodo newton raphson\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');

                while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  xi=(x0-(fx0)/(fx0d));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
            
              case 5
                %secante
                clc
                fprintf('metodo secante\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end
                Es=(0.5*10^(2-cifras));
                Ea=1000000;
                i=1;
                while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
                  x1=x2;
                  x2=xi;
                  if (i==1)
                    xar=xi;
                    i=i+1;
                  else 
                    Ea=(abs((xi-xar)/xi)*100);
                    xar=xi;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
              case 6
                %horner
                clc
                fprintf('metodo horner\n');
                cifras=input('ingrese el numero de cifras significativas: ');


                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while (x0<a || x0>b)
                    x0=input('ingrese el valor de x0: ');
                end

                Es=(0.5*10^(2-cifras));

                A=coeffs(f,'all');
                grado=numel(A)-1;

                for i=1:1:grado+1
                    if(i==1)
                       B(1)=A(1);
                    else
                        B(i)=A(i)+B(i-1)*x0;
                    end
                end

                R0=B(grado+1);

                for i=1:1:grado
                    if(i==1)
                       C(1)=B(1);
                    else
                        C(i)=B(i)+C(i-1)*x0;
                    end
                end

                S1=C(grado);


                xr=vpa(x0-(R0/S1));
                Ea=vpa(abs((xr-x0)/xr)*100);

                while(Ea>Es)
                    x0=xr;

                    for i=1:1:grado+1
                        if(i==1)
                           B(1)=A(1);
                        else
                            B(i)=A(i)+B(i-1)*x0;
                        end
                    end

                    R0=B(grado+1);

                    for i=1:1:grado
                        if(i==1)
                           C(1)=B(1);
                        else
                            C(i)=B(i)+C(i-1)*x0;
                        end
                    end

                    S1=C(grado);


                    xr=vpa(x0-(R0/S1));
                    Ea=vpa(abs((xr-x0)/xr)*100);

                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));

              case 7
                %muller
                clc
                fprintf('metodo muller\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                Es=(0.5*10^(2-cifras));

                %se piden los 3 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while (x2<a || x2>b)
                    x2=input('ingrese el valor de x2: ');
                end

                x3=input('ingrese el valor de x3: ');
                while (x3<a || x3>b)
                    x3=input('ingrese el valor de x3: ');
                end

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fx3=subs(f,x3);

                h0=x2-x1;
                h1=x3-x2;
                s0=(fx2-fx1)/h0;
                s1=(fx3-fx2)/h1;
                a=(s1-s0)/(h1-h0);
                b=a*h1+s1;
                c=fx3;

                D=(b^2-4*a*c)^(1/2);

                %evaluamos la condicion
                if (abs(b+D)>abs(b-D))
                    xr=x3+((-2*c)/(b+D));
                else
                    xr=x3+((-2*c)/(b-D));
                end

                Ea=abs((xr-x3)/xr)*100;
                while (Ea>Es)
                    x1=x2;
                    x2=x3;
                    x3=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    fx3=subs(f,x3);
                    h0=x2-x1;
                    h1=x3-x2;
                    s0=(fx2-fx1)/h0;
                    s1=(fx3-fx2)/h1;
                    a=(s1-s0)/(h1-h0);
                    b=a*h1+s1;
                    c=fx3;


                    D=vpa((b^2-4*a*c)^(1/2));


                    %evaluamos la condicion
                    if (abs(b+D)>abs(b-D))
                        xr=vpa(x3+((-2*c)/(b+D)));
                    else
                        xr=vpa(x3+((-2*c)/(b-D)));
                    end

                    Ea=abs((xr-x3)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error es: \n');
                disp(vpa(Ea));
                
            end
        else
            if(valoresNum(2)^2-4*valoresNum(1)*valoresNum(3))<0
                raiz1=complex(single(-valoresNum(2)/2*valoresNum(1)),single(((-(valoresNum(2)^2-4*valoresNum(1)*valoresNum(3)))^(1/2))/2*valoresNum(1)));
                raiz2=complex(single(-valoresNum(2)/2*valoresNum(1)),single(-((-(valoresNum(2)^2-4*valoresNum(1)*valoresNum(3)))^(1/2))/2*valoresNum(1)));
            else
                raiz1=(-valoresNum(2)+(valoresNum(2)^2-4*valoresNum(1)*valoresNum(3))^(1/2))/(2*valoresNum(1));
                raiz2=(-valoresNum(2)-(valoresNum(2)^2-4*valoresNum(1)*valoresNum(3))^(1/2))/(2*valoresNum(1));
            end
            fprintf('las raices son:\n');
            disp(vpa(raiz1,15));
            disp(vpa(raiz2,15));
        end
        
            
    elseif (grado==3)
        clc
          fprintf('Tipo de Metodos:\n1.Iterativos\n2.Directos\n');
          met=input('ingrese el tipo de metodo que desea: ');
          if (met==1)
              clc
            fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n7.Horner\n8.Muller\n9.Bairstow\n');
            iteMetho=input('ingrese el metodo iterativo que desea: ');
            switch (iteMetho)
              case 1
                %metodo biseccion:
                clc
                fprintf('metodo biseccion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                while (Ea>Es)
                  xr=((x1+x2)/2);
                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  fxr=subs(f,xr);
                  f1xr=(fx1*fxr);
                  if (f1xr>0)
                    x1=xr;
                  else 
                    x2=xr;
                  end
                  if (i==1)
                    xar=xr;
                    i=i+1;
                  else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(xr);
                fprintf('el error aproximado es: \n');
                disp(Ea);
                
              case 2
                %metodo falsa posicion:
                clc
                fprintf('metodo falsa posicion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end
                Ea=100000;

                while (Ea>Es)
                    xant=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                    fxr=subs(f,xr);

                    if (fx1*fxr<0)
                        x2 = xr;
                    else
                        if (fx1*fxr>0)
                            x1 = xr;
                        else
                            Ea=0;
                        end
                    end

                    Ea=abs((xr-xant)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
              case 3
                %punto fijo
                clc
                fprintf('metodo punto fijo\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se pide el valor de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while (x0<a || x0>b)
                    x0=input('ingrese el valor de x0: ');
                end

                %se pide la funcion g(x) 
                try
                g=input('ingrese la funcion g(x)= ');
                catch warning
                fprintf('');
                end

                dg=diff(g,x);

                Es=(0.5*10^(2-cifras));

                if (abs(subs(dg,x0))>1) 
                    fprintf('No hay convergencia\n');
                    Ea=0;
                    xr=0;
                else
                    xr=subs(g,x0);
                    Ea = abs((xr-x0)/xr)*100;
                end

                while Ea>Es
                    x0 = xr;
                    xr=subs(g,x0);
                    Ea = vpa(abs((xr-x0)/xr)*100);
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error es: \n');
                disp(vpa(Ea));
                
              case 4
                %Newton Raphson
                clc
                fprintf('metodo newton raphson\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');

                while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  xi=(x0-(fx0)/(fx0d));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
              case 5
                %Newton Raphson mejorado
                clc
                fprintf('metodo newton mejorado\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se pide valor de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while(x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                calculo=(0.5*10^(2-cifras));
                Es=calculo;
                Ea=10000000000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  fx0d2=subs(deriDos,x0);
                  xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
              case 6
                %secante
                clc
                fprintf('metodo secante\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end
                Es=(0.5*10^(2-cifras));
                Ea=1000000;
                i=1;
                while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
                  x1=x2;
                  x2=xi;
                  if (i==1)
                    xar=xi;
                    i=i+1;
                  else 
                    Ea=(abs((xi-xar)/xi)*100);
                    xar=xi;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
              case 7
                %horner
                clc
                fprintf('metodo horner\n');
                cifras=input('ingrese el numero de cifras significativas: ');


                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while (x0<a || x0>b)
                    x0=input('ingrese el valor de x0: ');
                end

                Es=(0.5*10^(2-cifras));

                A=coeffs(f,'all');
                grado=numel(A)-1;

                for i=1:1:grado+1
                    if(i==1)
                       B(1)=A(1);
                    else
                        B(i)=A(i)+B(i-1)*x0;
                    end
                end

                R0=B(grado+1);

                for i=1:1:grado
                    if(i==1)
                       C(1)=B(1);
                    else
                        C(i)=B(i)+C(i-1)*x0;
                    end
                end

                S1=C(grado);


                xr=vpa(x0-(R0/S1));
                Ea=vpa(abs((xr-x0)/xr)*100);

                while(Ea>Es)
                    x0=xr;

                    for i=1:1:grado+1
                        if(i==1)
                           B(1)=A(1);
                        else
                            B(i)=A(i)+B(i-1)*x0;
                        end
                    end

                    R0=B(grado+1);

                    for i=1:1:grado
                        if(i==1)
                           C(1)=B(1);
                        else
                            C(i)=B(i)+C(i-1)*x0;
                        end
                    end

                    S1=C(grado);


                    xr=vpa(x0-(R0/S1));
                    Ea=vpa(abs((xr-x0)/xr)*100);

                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
              case 8
                %muller
                clc
                fprintf('metodo muller\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                Es=(0.5*10^(2-cifras));

                %se piden los 3 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while (x2<a || x2>b)
                    x2=input('ingrese el valor de x2: ');
                end

                x3=input('ingrese el valor de x3: ');
                while (x3<a || x3>b)
                    x3=input('ingrese el valor de x3: ');
                end

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fx3=subs(f,x3);

                h0=x2-x1;
                h1=x3-x2;
                s0=(fx2-fx1)/h0;
                s1=(fx3-fx2)/h1;
                a=(s1-s0)/(h1-h0);
                b=a*h1+s1;
                c=fx3;

                D=(b^2-4*a*c)^(1/2);

                %evaluamos la condicion
                if (abs(b+D)>abs(b-D))
                    xr=x3+((-2*c)/(b+D));
                else
                    xr=x3+((-2*c)/(b-D));
                end

                Ea=abs((xr-x3)/xr)*100;
                while (Ea>Es)
                    x1=x2;
                    x2=x3;
                    x3=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    fx3=subs(f,x3);
                    h0=x2-x1;
                    h1=x3-x2;
                    s0=(fx2-fx1)/h0;
                    s1=(fx3-fx2)/h1;
                    a=(s1-s0)/(h1-h0);
                    b=a*h1+s1;
                    c=fx3;


                    D=vpa((b^2-4*a*c)^(1/2));


                    %evaluamos la condicion
                    if (abs(b+D)>abs(b-D))
                        xr=vpa(x3+((-2*c)/(b+D)));
                    else
                        xr=vpa(x3+((-2*c)/(b-D)));
                    end

                    Ea=abs((xr-x3)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error es: \n');
                disp(vpa(Ea));
                
              case 9 
                %bairstow
                clc
                fprintf('metodo bairstow\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                Es=(0.5*10^(2-cifras));


                A=coeffs(f,'all');
                grado=numel(A)-1;
                R0=input('ingrese el valor de R: ');
                S0=input('ingrese el valor de S: ');

                %repetitivo
                while(grado>0)

                    if(grado==1)
                        raices(grado)=-A(grado+1)/A(grado);
                        grado=grado-1;
                    else
                        if(grado==2)
                            if(A(grado)^2-4*A(grado-1)*A(grado+1))<0
                                raices(grado)=complex(single(-A(grado)/2*A(grado-1)),single(((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                                raices(grado-1)=complex(single(-A(grado)/2*A(grado-1)),single(-((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                            else
                                raices(grado)=(-A(grado)+(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                                raices(grado-1)=(-A(grado)-(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                            end
                            grado=grado-2;
                        else

                            for i=1:1:grado+1
                               if i==1
                                   B(1)=vpa(A(1),15);
                               else
                                   if i==2 
                                       B(2)=vpa(A(2)+B(1)*R0,15);
                                   else
                                       B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                                   end
                               end
                            end

                            for i=1:1:grado
                               if i==1
                                   C(1)=vpa(B(1),15);
                               else
                                   if i==2 
                                       C(2)=vpa(B(2)+C(1)*R0,15);
                                   else
                                       C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                                   end
                               end
                            end

                            deltaR=(((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
                            deltaS=(((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
                            R=R0+deltaR;
                            S=S0+deltaS;

                            Ear=abs(deltaR/R)*100;
                            Eas=abs(deltaS/S)*100;

                            while (Ear>Es || Eas>Es)
                                R0=R;
                                S0=S;

                                for i=1:1:grado+1
                                   if i==1
                                       B(1)=vpa(A(1),15);
                                   else
                                       if i==2 
                                           B(2)=vpa(A(2)+B(1)*R0,15);
                                       else
                                           B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                                       end
                                   end
                                end

                                for i=1:1:grado
                                   if i==1
                                       C(1)=vpa(B(1),15);
                                   else
                                       if i==2 
                                           C(2)=vpa(B(2)+C(1)*R0,15);
                                       else
                                           C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                                       end
                                   end
                                end

                                deltaR=vpa((((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);
                                deltaS=vpa((((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);

                                R=R0+deltaR;
                                S=S0+deltaS;

                                Ear=abs(deltaR/R)*100;
                                Eas=abs(deltaS/S)*100;

                            end

                            %verificar
                            if ((R^2+4*S)<0) 
                                    raices(grado)=complex(single(R/2),single(((-(R^2+4*S))^(1/2))/2));
                                    raices(grado-1)=complex(single(R/2),single(-((-(R^2+4*S))^(1/2))/2));
                            else
                                    raices(grado) = vpa((R+(R^2+4*S)^(1/2))/2,15);
                                    raices(grado-1) = vpa((R-(R^2+4*S)^(1/2))/2,15);
                            end

                            grado=grado-2;

                            for i=1:1:grado+1
                                if i==1
                                   A(1)=A(1);
                                else
                                    if i==2 
                                        A(2)=vpa(A(2)+A(1)*R);
                                    else
                                        A(i)=vpa(A(i)+A(i-1)*R+A(i-2)*S);
                                    end
                                end
                            end

                        end
                    end  


                end

                fprintf('Las raices son:\n');
                for i=1:1:numel(raices)
                    disp(vpa(raices(i)));
                end
                
            end

          else
            %tartaglia               
            clc
            fprintf('El metodo para esta funcion es Tartaglia\n');
            %calculos pertinentes...
            valoresNum=coeffs(f,'all');
            w=numel(valoresNum);
            grado=w-1;


            s=valoresNum(1,1);
            if (s==1)
              %caso tartaglia
              a=valoresNum(1,2);
              b=valoresNum(1,3);
              c=valoresNum(1,4);
            else
              a=((valoresNum(1,2))/s);
              b=((valoresNum(1,3))/s);
              c=((valoresNum(1,4))/s);
            end
            k=(-a/3);

            P=(b-((a^2)/3));
            Q=(c-((a*b)/3)+((2*(a^3))/27));
            E=(((4*P^3+27*Q^2)/108));

            if (E<0)
              fi=acos((sqrt(27)*Q)/(2*P*sqrt((-P))));
              x1=((2*sqrt(-P/3)*cos(fi/3))+k);
                x2=((2*sqrt(-P/3)*cos((fi/3)+((2*pi)/3)))+k);
                x3=((2*sqrt(-P/3)*cos((fi/3)+((4*pi)/3)))+k);
              fprintf('Las raices son:\n');
              fprintf('x= ');
              disp(vpa(x1));
              fprintf('\nx= ');
              disp(vpa(x2));
              fprintf('\nx= ');
              disp(vpa(x3));

            else 
              D=sqrt(E);
              if (D>0)
                A=double(-Q/2+D);
                B=double(-Q/2-D);

                x1=((nthroot(A,3)+nthroot(B,3))+k);
                x2a=(-1*((nthroot(A,3)+nthroot(B,3))/2)+k);
                x2b=((-sqrt(3)*((nthroot(A,3)-nthroot(B,3))/2)));
                x3a=(-1*((nthroot(A,3)+nthroot(B,3))/2)+k);
                x3b=((-sqrt(3)*((nthroot(A,3)-nthroot(B,3))/2)));
                fprintf('Las raices son:\n');
                fprintf('x= ');
                disp(vpa(x1));

                fprintf('\nx= ');
                disp(vpa(complex(double(x2a),double(x2b))));

                fprintf('\nx= ');
                disp(vpa(complex(double(x3a),double(-x3b))));
              else 
                if (Q>0)
                  xx=double((Q)/2);
                  xs=nthroot(vpa(xx),3);
                  xd=-xs;
                  x1=double(2*xd+k);
                  x2=nthroot(((Q)/2),3)+k;
                  x3=nthroot(((Q)/2),3)+k;
                else 
                  xx=double((-Q)/2);
                  xs=nthroot(vpa(xx),3);
                  x1=double(2*xs+k);
                  x2=nthroot(double((Q)/2),3)+k;
                  x3=nthroot(double((Q)/2),3)+k;
                end
                fprintf('Las raices son:\n');
                fprintf('x= ');
                disp(vpa(x1));
                fprintf('\nx= ');
                disp(vpa(x2));
                fprintf('\nx= ');
                disp(vpa(x3));
              end
            end
            
          end     
          
   elseif (grado==4)
       clc
          fprintf('Tipo de Metodos:\n1.Iterativos\n2.Directos\n');
          met=input('ingrese el tipo de metodo que desea: ');
          if (met==1)
              clc
            fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n7.Horner\n8.Muller\n9.Bairstow\n');
            iteMetho=input('ingrese el metodo iterativo que desea: ');
            switch (iteMetho)
              case 1
                %metodo biseccion:
                clc
                fprintf('metodo biseccion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                while (Ea>Es)
                  xr=((x1+x2)/2);
                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  fxr=subs(f,xr);
                  f1xr=(fx1*fxr);
                  if (f1xr>0)
                    x1=xr;
                  else 
                    x2=xr;
                  end
                  if (i==1)
                    xar=xr;
                    i=i+1;
                  else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(xr);
                fprintf('el error aproximado es: \n');
                disp(Ea);
                
              case 2
                %metodo falsa posicion:
                clc
                fprintf('metodo falsa posicion\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end

                Es=(0.5*10^(2-cifras));

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end
                Ea=100000;

                while (Ea>Es)
                    xant=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                    fxr=subs(f,xr);

                    if (fx1*fxr<0)
                        x2 = xr;
                    else
                        if (fx1*fxr>0)
                            x1 = xr;
                        else
                            Ea=0;
                        end
                    end

                    Ea=abs((xr-xant)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
                
              case 3
                %punto fijo
                clc
                fprintf('metodo punto fijo\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                
                %se pide el valor de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while (x0<a || x0>b)
                    x0=input('ingrese el valor de x0: ');
                end

                %se pide la funcion g(x) 
                try
                g=input('ingrese la funcion g(x)= ');
                catch warning
                fprintf('');
                end

                dg=diff(g,x);

                Es=(0.5*10^(2-cifras));

                if (abs(subs(dg,x0))>1) 
                    fprintf('No hay convergencia\n');
                    Ea=0;
                    xr=0;
                else
                    xr=subs(g,x0);
                    Ea = abs((xr-x0)/xr)*100;
                end

                while Ea>Es
                    x0 = xr;
                    xr=subs(g,x0);
                    Ea = vpa(abs((xr-x0)/xr)*100);
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error es: \n');
                disp(vpa(Ea));
                
              case 4
                %Newton Raphson
                clc
                fprintf('metodo newton raphson\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');

                while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                Es=(0.5*10^(2-cifras));
                Ea=100000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  xi=(x0-(fx0)/(fx0d));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
              case 5
                %Newton Raphson mejorado
                clc
                fprintf('metodo newton mejorado\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se pide valor de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while(x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
                end
                %verfica la convergencia
                deriUno=diff(f,x);
                deriDos=diff(deriUno,x);
                fcero=subs(f,x0);
                fderiu=subs(deriUno,x0);
                fderid=subs(deriDos,x0);
                convergencia=(abs((fcero*fderid)/(fderiu^2)));
                calculo=(0.5*10^(2-cifras));
                Es=calculo;
                Ea=10000000000;
                i=1;
                if (convergencia<1)
                  while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx0=subs(f,x0);
                  fx0d=subs(deriUno,x0);
                  fx0d2=subs(deriDos,x0);
                  xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
                  Ea=(abs((xi-x0)/xi)*100);
                  x0=xi;
                  end
                else 
                  fprintf('no existe convergencia\n');
                  xi=0;
                  Ea=0;
                end
                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error es: \n');
                disp(vpa(Ea));
              case 6
                %secante
                clc
                fprintf('metodo secante\n');
                cifras=input('ingrese el numero de cifras significativas: ');

                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while ((x1<a || x1>b) || x2<x1)
                    x2=input('ingrese el valor de x2: ');
                end
                Es=(0.5*10^(2-cifras));
                Ea=1000000;
                i=1;
                while (Ea>Es)

                  %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                  fx1=subs(f,x1);
                  fx2=subs(f,x2);
                  xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
                  x1=x2;
                  x2=xi;
                  if (i==1)
                    xar=xi;
                    i=i+1;
                  else 
                    Ea=(abs((xi-xar)/xi)*100);
                    xar=xi;
                    i=i+1;
                  end
                end

                fprintf('el resultado es:\n');
                disp(vpa(xi));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
            
              case 7
                %horner
                clc
                fprintf('metodo horner\n');
                cifras=input('ingrese el numero de cifras significativas: ');


                %se piden los 2 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                x0=input('ingrese el valor de x0: ');
                while (x0<a || x0>b)
                    x0=input('ingrese el valor de x0: ');
                end

                Es=(0.5*10^(2-cifras));

                A=coeffs(f,'all');
                grado=numel(A)-1;

                for i=1:1:grado+1
                    if(i==1)
                       B(1)=A(1);
                    else
                        B(i)=A(i)+B(i-1)*x0;
                    end
                end

                R0=B(grado+1);

                for i=1:1:grado
                    if(i==1)
                       C(1)=B(1);
                    else
                        C(i)=B(i)+C(i-1)*x0;
                    end
                end

                S1=C(grado);


                xr=vpa(x0-(R0/S1));
                Ea=vpa(abs((xr-x0)/xr)*100);

                while(Ea>Es)
                    x0=xr;

                    for i=1:1:grado+1
                        if(i==1)
                           B(1)=A(1);
                        else
                            B(i)=A(i)+B(i-1)*x0;
                        end
                    end

                    R0=B(grado+1);

                    for i=1:1:grado
                        if(i==1)
                           C(1)=B(1);
                        else
                            C(i)=B(i)+C(i-1)*x0;
                        end
                    end

                    S1=C(grado);


                    xr=vpa(x0-(R0/S1));
                    Ea=vpa(abs((xr-x0)/xr)*100);

                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error aproximado es: \n');
                disp(vpa(Ea));
              case 8
                %muller
                clc
                fprintf('metodo muller\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                Es=(0.5*10^(2-cifras));

                %se piden los 3 valores de inicio para el metodo 
                fprintf('en base a la grafica ingrese los siguientes datos:\n');
                 x1=input('ingrese el valor de x1: ');
                while (x1<a || x1>b)
                    x1=input('ingrese el valor de x1: ');
                end

                x2=input('ingrese el valor de x2: ');
                while (x2<a || x2>b)
                    x2=input('ingrese el valor de x2: ');
                end

                x3=input('ingrese el valor de x3: ');
                while (x3<a || x3>b)
                    x3=input('ingrese el valor de x3: ');
                end

                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fx3=subs(f,x3);

                h0=x2-x1;
                h1=x3-x2;
                s0=(fx2-fx1)/h0;
                s1=(fx3-fx2)/h1;
                a=(s1-s0)/(h1-h0);
                b=a*h1+s1;
                c=fx3;

                D=(b^2-4*a*c)^(1/2);

                %evaluamos la condicion
                if (abs(b+D)>abs(b-D))
                    xr=x3+((-2*c)/(b+D));
                else
                    xr=x3+((-2*c)/(b-D));
                end

                Ea=abs((xr-x3)/xr)*100;
                while (Ea>Es)
                    x1=x2;
                    x2=x3;
                    x3=xr;
                    fx1=subs(f,x1);
                    fx2=subs(f,x2);
                    fx3=subs(f,x3);
                    h0=x2-x1;
                    h1=x3-x2;
                    s0=(fx2-fx1)/h0;
                    s1=(fx3-fx2)/h1;
                    a=(s1-s0)/(h1-h0);
                    b=a*h1+s1;
                    c=fx3;


                    D=vpa((b^2-4*a*c)^(1/2));


                    %evaluamos la condicion
                    if (abs(b+D)>abs(b-D))
                        xr=vpa(x3+((-2*c)/(b+D)));
                    else
                        xr=vpa(x3+((-2*c)/(b-D)));
                    end

                    Ea=abs((xr-x3)/xr)*100;
                end

                fprintf('el resultado es:\n');
                disp(vpa(xr));
                fprintf('el error es: \n');
                disp(vpa(Ea));
            
              case 9 
                %bairstow
                clc
                fprintf('metodo bairstow\n');
                cifras=input('ingrese el numero de cifras significativas: ');
                Es=(0.5*10^(2-cifras));


                A=coeffs(f,'all');
                grado=numel(A)-1;
                R0=input('ingrese el valor de R: ');
                S0=input('ingrese el valor de S: ');

                %repetitivo
                while(grado>0)

                    if(grado==1)
                        raices(grado)=-A(grado+1)/A(grado);
                        grado=grado-1;
                    else
                        if(grado==2)
                            if(A(grado)^2-4*A(grado-1)*A(grado+1))<0
                                raices(grado)=complex(single(-A(grado)/2*A(grado-1)),single(((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                                raices(grado-1)=complex(single(-A(grado)/2*A(grado-1)),single(-((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                            else
                                raices(grado)=(-A(grado)+(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                                raices(grado-1)=(-A(grado)-(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                            end
                            grado=grado-2;
                        else

                            for i=1:1:grado+1
                               if i==1
                                   B(1)=vpa(A(1),15);
                               else
                                   if i==2 
                                       B(2)=vpa(A(2)+B(1)*R0,15);
                                   else
                                       B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                                   end
                               end
                            end

                            for i=1:1:grado
                               if i==1
                                   C(1)=vpa(B(1),15);
                               else
                                   if i==2 
                                       C(2)=vpa(B(2)+C(1)*R0,15);
                                   else
                                       C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                                   end
                               end
                            end

                            deltaR=(((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
                            deltaS=(((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
                            R=R0+deltaR;
                            S=S0+deltaS;

                            Ear=abs(deltaR/R)*100;
                            Eas=abs(deltaS/S)*100;

                            while (Ear>Es || Eas>Es)
                                R0=R;
                                S0=S;

                                for i=1:1:grado+1
                                   if i==1
                                       B(1)=vpa(A(1),15);
                                   else
                                       if i==2 
                                           B(2)=vpa(A(2)+B(1)*R0,15);
                                       else
                                           B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                                       end
                                   end
                                end

                                for i=1:1:grado
                                   if i==1
                                       C(1)=vpa(B(1),15);
                                   else
                                       if i==2 
                                           C(2)=vpa(B(2)+C(1)*R0,15);
                                       else
                                           C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                                       end
                                   end
                                end

                                deltaR=vpa((((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);
                                deltaS=vpa((((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);

                                R=R0+deltaR;
                                S=S0+deltaS;

                                Ear=abs(deltaR/R)*100;
                                Eas=abs(deltaS/S)*100;

                            end

                            %verificar
                            if ((R^2+4*S)<0) 
                                    raices(grado)=complex(single(R/2),single(((-(R^2+4*S))^(1/2))/2));
                                    raices(grado-1)=complex(single(R/2),single(-((-(R^2+4*S))^(1/2))/2));
                            else
                                    raices(grado) = vpa((R+(R^2+4*S)^(1/2))/2,15);
                                    raices(grado-1) = vpa((R-(R^2+4*S)^(1/2))/2,15);
                            end

                            grado=grado-2;

                            for i=1:1:grado+1
                                if i==1
                                   A(1)=A(1);
                                else
                                    if i==2 
                                        A(2)=vpa(A(2)+A(1)*R);
                                    else
                                        A(i)=vpa(A(i)+A(i-1)*R+A(i-2)*S);
                                    end
                                end
                            end

                        end
                    end  


                end

                fprintf('Las raices son:\n');
                for i=1:1:numel(raices)
                    disp(vpa(raices(i)));
                end
                
                
            end
          else 
            clc
            fprintf('El metodo para esta funcion es Ferrari\n');
            %calculos pertinentes...xd
            valoresNum=coeffs(f,'all');
            w=numel(valoresNum);
            grado=w-1;
            s=valoresNum(1,1);
            if (s==1)
              %caso ferrari
              a=valoresNum(1,2);
              b=valoresNum(1,3);
              c=valoresNum(1,4);
              d=valoresNum(1,5);
            else
              a=((valoresNum(1,2))/s);
              b=((valoresNum(1,3))/s);
              c=((valoresNum(1,4))/s);
              d=((valoresNum(1,5))/s);
            end
            K=(-a/4);
            P=((8*b-3*a^2)/8);
            Q=((8*c-4*a*b+a^3)/8);
            R=((256*d-64*a*c+16*a^2*b-3*a^4)/256);

            ap=(-P/2);
            bp=(-R);
            cp=((4*P*R-Q^2)/8);

            Pp=(bp-((ap^2)/3));
            Qp=(cp-((ap*bp)/3)+((2*(ap^3))/27));

            E=(((4*Pp^3+27*Qp^2)/108));
            kp=(-ap/3);

            if (E<0)
              fi=acos((sqrt(27)*Qp)/(2*Pp*sqrt((-Pp))));
              U=((2*sqrt(-Pp/3)*cos(fi/3))+kp);
                V=(sqrt(2*U-P));
              W=((Q)/(-2*V));
              x1=(((V+sqrt(V^2-4*(U-W)))/(2))+K);
                x2=(((V-sqrt(V^2-4*(U-W)))/(2))+K);
                x3=(((-V+sqrt(V^2-4*(U+W)))/(2))+K);
                x4=(((-V-sqrt(V^2-4*(U+W)))/(2))+K);
              fprintf('Las raices son:\n');
              fprintf('x= ');
              disp(vpa(x1));
              fprintf('\nx= ');
              disp(vpa(x2));
              fprintf('\nx= ');
              disp(vpa(x3));
              fprintf('\nx= ');
              disp(vpa(x4));
            else 
              D=sqrt(E);
              if (D>0)
                A=vpa(double(-Qp/2+D))
                B=vpa(double(-Qp/2-D))

                if (A<0 & B<0)
                  Ag=double(-A);
                  Bg=double(-B);
                  Fg=nthroot(Ag,3)+nthroot(Bg,3);
                  U=vpa((-1*Fg)+kp)
                  V=vpa((sqrt(2*U-P)))
                  W=vpa(((Q)/(-2*V)))
                elseif (A<0)
                  Ag=-A;
                  Fg=nthroot(Ag,3);
                  fd=(-1*Fg+nthroot(B,3));
                  U=vpa((fd)+kp)
                  V=vpa((sqrt(2*U-P)))
                  W=vpa(((Q)/(-2*V)))
                elseif (B<0)
                  Bg=-B;
                  Fg=nthroot(Bg,3);
                  fd=(-1*Fg+nthroot(A,3));
                  U=vpa((fd)+kp)
                  V=vpa((sqrt(2*U-P)))
                  W=vpa(((Q)/(-2*V)))

                else 
                  U=vpa(((nthroot(A,3)+nthroot(B,3))+kp))
                  V=vpa((sqrt(2*U-P)))
                  W=vpa(((Q)/(-2*V)))

                end
                  x1=(((V+sqrt(V^2-4*(U-W)))/(2))+K);
                    x2=(((V-sqrt(V^2-4*(U-W)))/(2))+K);
                    x3=(((-V+sqrt(V^2-4*(U+W)))/(2))+K);
                  x4=(((-V-sqrt(V^2-4*(U+W)))/(2))+K);
                  fprintf('Las raices son:\n');
                  fprintf('x= ');
                  disp(vpa(x1));
                  fprintf('\nx= ');
                  disp(vpa(x2));
                  fprintf('\nx= ');
                  disp(vpa(x3));
                  fprintf('\nx= ');
                  disp(vpa(x4));


              else 
                if (Qp>0)
                  xx=double((Qp)/2);
                  xs=nthroot(double(xx),3);
                  xd=-xs;
                  U=(2*xd+kp);

                else 
                  xx=((-Q)/2);
                  xs=nthroot(double(xx),3);
                  U=(2*xs+kp);

                end

                V=(sqrt(2*U-P));
                W=((Q)/(-2*V));
                x1=(((V+sqrt(V^2-4*(U-W)))/(2))+K);
                  x2=(((V-sqrt(V^2-4*(U-W)))/(2))+K);
                  x3=(((-V+sqrt(V^2-4*(U+W)))/(2))+K);
                x4=(((-V-sqrt(V^2-4*(U+W)))/(2))+K);
                fprintf('Las raices son:\n');
                fprintf('x= ');
                disp(vpa(x1));
                fprintf('\nx= ');
                disp(vpa(x2));
                fprintf('\nx= ');
                disp(vpa(x3));
                fprintf('\nx= ');
                disp(vpa(x4));

              end
            end
          end
    else
    %Es de grado 5 o mayor
    clc
    fprintf('Metodos iterativos\n');
    fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n7.Horner\n8.Muller\n9.Bairstow\n'); 
    iteMetho=input('ingrese el metodo iterativo que desea: ');
        switch (iteMetho)
          case 1
            %metodo biseccion:
            clc
            fprintf('metodo biseccion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            while (Ea>Es)
                xr=((x1+x2)/2);
                %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fxr=subs(f,xr);
                f1xr=(fx1*fxr);
                if (f1xr>0)
                    x1=xr;
                else 
                    x2=xr;
                end
                if (i==1)
                    xar=xr;
                    i=i+1;
                else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                end
            end

            fprintf('el resultado es:\n');
            disp(xr);
            fprintf('el error aproximado es: \n');
            disp(Ea);
            
          case 2
            %metodo falsa posicion:
            clc
            fprintf('metodo falsa posicion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));

            fx1=subs(f,x1);
            fx2=subs(f,x2);
            xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
            fxr=subs(f,xr);

            if (fx1*fxr<0)
                x2 = xr;
            else
                if (fx1*fxr>0)
                    x1 = xr;
                else
                    Ea=0;
                end
            end
            Ea=100000;

            while (Ea>Es)
                xant=xr;
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end

                Ea=abs((xr-xant)/xr)*100;
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
          case 3
            %punto fijo
            clc
            fprintf('metodo punto fijo\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se pide el valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
            end

            %se pide la funcion g(x) 
            try
            g=input('ingrese la funcion g(x)= ');
            catch warning
            fprintf('');
            end

            dg=diff(g,x);

            Es=(0.5*10^(2-cifras));

            if (abs(subs(dg,x0))>1) 
                fprintf('No hay convergencia\n');
                Ea=0;
                xr=0;
            else
                xr=subs(g,x0);
                Ea = abs((xr-x0)/xr)*100;
            end

            while Ea>Es
                x0 = xr;
                xr=subs(g,x0);
                Ea = vpa(abs((xr-x0)/xr)*100);
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 4
            %Newton Raphson
            clc
            fprintf('metodo newton raphson\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');

            while (x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              xi=(x0-(fx0)/(fx0d));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 5
            %Newton Raphson mejorado
            clc
            fprintf('metodo newton mejorado\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se pide valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while(x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            calculo=(0.5*10^(2-cifras));
            Es=calculo;
            Ea=10000000000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              fx0d2=subs(deriDos,x0);
              xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 6
            %secante
            clc
            fprintf('metodo secante\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
             x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end
            Es=(0.5*10^(2-cifras));
            Ea=1000000;
            i=1;
            while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx1=subs(f,x1);
              fx2=subs(f,x2);
              xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
              x1=x2;
              x2=xi;
              if (i==1)
                xar=xi;
                i=i+1;
              else 
                Ea=(abs((xi-xar)/xi)*100);
                xar=xi;
                i=i+1;
              end
            end

            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
          case 7
            %horner
            clc
            fprintf('metodo horner\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
            end

            Es=(0.5*10^(2-cifras));

            A=coeffs(f,'all');
            grado=numel(A)-1;

            for i=1:1:grado+1
                if(i==1)
                   B(1)=A(1);
                else
                    B(i)=A(i)+B(i-1)*x0;
                end
            end

            R0=B(grado+1);

            for i=1:1:grado
                if(i==1)
                   C(1)=B(1);
                else
                    C(i)=B(i)+C(i-1)*x0;
                end
            end

            S1=C(grado);


            xr=vpa(x0-(R0/S1));
            Ea=vpa(abs((xr-x0)/xr)*100);

            while(Ea>Es)
                x0=xr;

                for i=1:1:grado+1
                    if(i==1)
                       B(1)=A(1);
                    else
                        B(i)=A(i)+B(i-1)*x0;
                    end
                end

                R0=B(grado+1);

                for i=1:1:grado
                    if(i==1)
                       C(1)=B(1);
                    else
                        C(i)=B(i)+C(i-1)*x0;
                    end
                end

                S1=C(grado);


                xr=vpa(x0-(R0/S1));
                Ea=vpa(abs((xr-x0)/xr)*100);

            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));

          case 8
            %muller
            clc
            fprintf('metodo muller\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            Es=(0.5*10^(2-cifras));
            
            %se piden los 3 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
             x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while (x2<a || x2>b)
                x2=input('ingrese el valor de x2: ');
            end

            x3=input('ingrese el valor de x3: ');
            while (x3<a || x3>b)
                x3=input('ingrese el valor de x3: ');
            end

            fx1=subs(f,x1);
            fx2=subs(f,x2);
            fx3=subs(f,x3);

            h0=x2-x1;
            h1=x3-x2;
            s0=(fx2-fx1)/h0;
            s1=(fx3-fx2)/h1;
            a=(s1-s0)/(h1-h0);
            b=a*h1+s1;
            c=fx3;

            D=(b^2-4*a*c)^(1/2);

            %evaluamos la condicion
            if (abs(b+D)>abs(b-D))
                xr=x3+((-2*c)/(b+D));
            else
                xr=x3+((-2*c)/(b-D));
            end

            Ea=abs((xr-x3)/xr)*100;
            while (Ea>Es)
                x1=x2;
                x2=x3;
                x3=xr;
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fx3=subs(f,x3);
                h0=x2-x1;
                h1=x3-x2;
                s0=(fx2-fx1)/h0;
                s1=(fx3-fx2)/h1;
                a=(s1-s0)/(h1-h0);
                b=a*h1+s1;
                c=fx3;


                D=vpa((b^2-4*a*c)^(1/2));


                %evaluamos la condicion
                if (abs(b+D)>abs(b-D))
                    xr=vpa(x3+((-2*c)/(b+D)));
                else
                    xr=vpa(x3+((-2*c)/(b-D)));
                end

                Ea=abs((xr-x3)/xr)*100;
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 9
            %bairstow  
            clc
            fprintf('metodo bairstow\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            Es=(0.5*10^(2-cifras));


            A=coeffs(f,'all');
            grado=numel(A)-1;
            R0=input('ingrese el valor de R: ');
            S0=input('ingrese el valor de S: ');

            %repetitivo
            while(grado>0)

                if(grado==1)
                    raices(grado)=-A(grado+1)/A(grado);
                    grado=grado-1;
                else
                    if(grado==2)
                        if(A(grado)^2-4*A(grado-1)*A(grado+1))<0
                            raices(grado)=complex(single(-A(grado)/2*A(grado-1)),single(((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                            raices(grado-1)=complex(single(-A(grado)/2*A(grado-1)),single(-((-(A(grado)^2-4*A(grado-1)*A(grado+1)))^(1/2))/2*A(grado-1)));
                        else
                            raices(grado)=(-A(grado)+(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                            raices(grado-1)=(-A(grado)-(A(grado)^2-4*A(grado-1)*A(grado+1))^(1/2))/(2*A(grado-1));
                        end
                        grado=grado-2;
                    else

                        for i=1:1:grado+1
                           if i==1
                               B(1)=vpa(A(1),15);
                           else
                               if i==2 
                                   B(2)=vpa(A(2)+B(1)*R0,15);
                               else
                                   B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                               end
                           end
                        end

                        for i=1:1:grado
                           if i==1
                               C(1)=vpa(B(1),15);
                           else
                               if i==2 
                                   C(2)=vpa(B(2)+C(1)*R0,15);
                               else
                                   C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                               end
                           end
                        end

                        deltaR=(((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
                        deltaS=(((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2)));
                        R=R0+deltaR;
                        S=S0+deltaS;

                        Ear=abs(deltaR/R)*100;
                        Eas=abs(deltaS/S)*100;

                        while (Ear>Es || Eas>Es)
                            R0=R;
                            S0=S;

                            for i=1:1:grado+1
                               if i==1
                                   B(1)=vpa(A(1),15);
                               else
                                   if i==2 
                                       B(2)=vpa(A(2)+B(1)*R0,15);
                                   else
                                       B(i)=vpa(A(i)+B(i-1)*R0+B(i-2)*S0,15);
                                   end
                               end
                            end

                            for i=1:1:grado
                               if i==1
                                   C(1)=vpa(B(1),15);
                               else
                                   if i==2 
                                       C(2)=vpa(B(2)+C(1)*R0,15);
                                   else
                                       C(i)=vpa(B(i)+C(i-1)*R0+C(i-2)*S0,15);
                                   end
                               end
                            end

                            deltaR=vpa((((-1)*B(grado)*C(grado-1))+(B(grado+1)*C(grado-2)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);
                            deltaS=vpa((((-1)*C(grado-1)*B(grado+1))+(C(grado)*B(grado)))/((C(grado-1)*C(grado-1))-(C(grado)*C(grado-2))),15);

                            R=R0+deltaR;
                            S=S0+deltaS;

                            Ear=abs(deltaR/R)*100;
                            Eas=abs(deltaS/S)*100;

                        end

                        %verificar
                        if ((R^2+4*S)<0) 
                                raices(grado)=complex(single(R/2),single(((-(R^2+4*S))^(1/2))/2));
                                raices(grado-1)=complex(single(R/2),single(-((-(R^2+4*S))^(1/2))/2));
                        else
                                raices(grado) = vpa((R+(R^2+4*S)^(1/2))/2,15);
                                raices(grado-1) = vpa((R-(R^2+4*S)^(1/2))/2,15);
                        end

                        grado=grado-2;

                        for i=1:1:grado+1
                            if i==1
                               A(1)=A(1);
                            else
                                if i==2 
                                    A(2)=vpa(A(2)+A(1)*R);
                                else
                                    A(i)=vpa(A(i)+A(i-1)*R+A(i-2)*S);
                                end
                            end
                        end

                    end
                end  


            end

            fprintf('Las raices son:\n');
            for i=1:1:numel(raices)
                disp(vpa(raices(i)));
            end
            
        end
    end
          
elseif (tipof==2)
    clc
    try
    f=input('ingrese la funcion trigonometrica. Ejems(sin(x); asin(x)); f(x)= ');
    catch warning
    fprintf('');
    end
    %aqui se pide el intervalo [a,b]
    a=input('ingrese el intervalo inferior: ');
    b=input('ingrese el intervalo superior: ');
    %esto de aqui grafica la funcion de manera simple
    ezplot(f,[a,b]);
    ylabel('Y');
    xlabel('X');
    grid on 
    hold on
    clc
    fprintf('Metodos iterativos\n');
    fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n'); 
    iteMetho=input('ingrese el metodo iterativo que desea: ');
        switch (iteMetho)
          case 1
            %metodo biseccion:
            clc
            fprintf('metodo biseccion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            while (Ea>Es)
                xr=((x1+x2)/2);
                %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fxr=subs(f,xr);
                f1xr=(fx1*fxr);
                if (f1xr>0)
                    x1=xr;
                else 
                    x2=xr;
                end
                if (i==1)
                    xar=xr;
                    i=i+1;
                else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                end
            end

            fprintf('el resultado es:\n');
            disp(xr);
            fprintf('el error aproximado es: \n');
            disp(Ea);
            
          case 2
            %metodo falsa posicion:
            clc
            fprintf('metodo falsa posicion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));

            fx1=subs(f,x1);
            fx2=subs(f,x2);
            xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
            fxr=subs(f,xr);

            if (fx1*fxr<0)
                x2 = xr;
            else
                if (fx1*fxr>0)
                    x1 = xr;
                else
                    Ea=0;
                end
            end
            Ea=100000;

            while (Ea>Es)
                xant=xr;
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end

                Ea=abs((xr-xant)/xr)*100;
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
          case 3
            %punto fijo
            clc
            fprintf('metodo punto fijo\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se pide el valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
            end

            %se pide la funcion g(x) 
            try
            g=input('ingrese la funcion g(x)= ');
            catch warning
            fprintf('');
            end

            dg=diff(g,x);

            Es=(0.5*10^(2-cifras));

            if (abs(subs(dg,x0))>1) 
                fprintf('No hay convergencia\n');
                Ea=0;
                xr=0;
            else
                xr=subs(g,x0);
                Ea = abs((xr-x0)/xr)*100;
            end

            while Ea>Es
                x0 = xr;
                xr=subs(g,x0);
                Ea = vpa(abs((xr-x0)/xr)*100);
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 4
            %Newton Raphson  
            clc
            fprintf('metodo newton raphson\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');

            while (x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              xi=(x0-(fx0)/(fx0d));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 5
            %Newton Raphson mejorado
            clc
            fprintf('metodo newton mejorado\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se pide valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while(x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            calculo=(0.5*10^(2-cifras));
            Es=calculo;
            Ea=10000000000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              fx0d2=subs(deriDos,x0);
              xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 6
            %secante
            clc
            fprintf('metodo secante\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
             x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end
            Es=(0.5*10^(2-cifras));
            Ea=1000000;
            i=1;
            while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx1=subs(f,x1);
              fx2=subs(f,x2);
              xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
              x1=x2;
              x2=xi;
              if (i==1)
                xar=xi;
                i=i+1;
              else 
                Ea=(abs((xi-xar)/xi)*100);
                xar=xi;
                i=i+1;
              end
            end

            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
        end
    
elseif (tipof==3)
    clc
    try
    f=input('ingrese la funcion algebraicas racionales e irracionales. Ejem( sqrt(x); (1/x) ); f(x)= ');
    catch warning
    fprintf('');
    end
    %aqui se pide el intervalo [a,b]
    a=input('ingrese el intervalo inferior: ');
    b=input('ingrese el intervalo superior: ');
    %esto de aqui grafica la funcion de manera simple
    ezplot(f,[a,b]);
    ylabel('Y');
    xlabel('X');
    grid on 
    hold on
    clc
    fprintf('Metodos iterativos\n');
    fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n'); 
    iteMetho=input('ingrese el metodo iterativo que desea: ');
        switch (iteMetho)
          case 1
            %metodo biseccion:
            clc
            fprintf('metodo biseccion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            while (Ea>Es)
                xr=((x1+x2)/2);
                %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fxr=subs(f,xr);
                f1xr=(fx1*fxr);
                if (f1xr>0)
                    x1=xr;
                else 
                    x2=xr;
                end
                if (i==1)
                    xar=xr;
                    i=i+1;
                else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                end
            end

            fprintf('el resultado es:\n');
            disp(xr);
            fprintf('el error aproximado es: \n');
            disp(Ea);
          case 2
            %metodo falsa posicion:
            clc
            fprintf('metodo falsa posicion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));

            fx1=subs(f,x1);
            fx2=subs(f,x2);
            xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
            fxr=subs(f,xr);

            if (fx1*fxr<0)
                x2 = xr;
            else
                if (fx1*fxr>0)
                    x1 = xr;
                else
                    Ea=0;
                end
            end
            Ea=100000;

            while (Ea>Es)
                xant=xr;
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end

                Ea=abs((xr-xant)/xr)*100;
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
          case 3
            %punto fijo
            clc
            fprintf('metodo punto fijo\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se pide el valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
            end

            %se pide la funcion g(x) 
            try
            g=input('ingrese la funcion g(x)= ');
            catch warning
            fprintf('');
            end

            dg=diff(g,x);

            Es=(0.5*10^(2-cifras));

            if (abs(subs(dg,x0))>1) 
                fprintf('No hay convergencia\n');
                Ea=0;
                xr=0;
            else
                xr=subs(g,x0);
                Ea = abs((xr-x0)/xr)*100;
            end

            while Ea>Es
                x0 = xr;
                xr=subs(g,x0);
                Ea = vpa(abs((xr-x0)/xr)*100);
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 4
            %Newton Raphson
            clc
            fprintf('metodo newton raphson\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');

            while (x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              xi=(x0-(fx0)/(fx0d));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 5
            %Newton Raphson mejorado
            clc
            fprintf('metodo newton mejorado\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se pide valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while(x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            calculo=(0.5*10^(2-cifras));
            Es=calculo;
            Ea=10000000000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              fx0d2=subs(deriDos,x0);
              xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 6
            %secante
            clc
            fprintf('metodo secante\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
             x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end
            Es=(0.5*10^(2-cifras));
            Ea=1000000;
            i=1;
            while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx1=subs(f,x1);
              fx2=subs(f,x2);
              xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
              x1=x2;
              x2=xi;
              if (i==1)
                xar=xi;
                i=i+1;
              else 
                Ea=(abs((xi-xar)/xi)*100);
                xar=xi;
                i=i+1;
              end
            end

            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
        end
elseif (tipof==4)
    clc
    try
    f=input('ingrese la funcion logaritmica. Ejem( log(x)(este es ln(x)); log10(x); log2(x) ); f(x)= ');
    catch warning
    fprintf('');
    end
    %aqui se pide el intervalo [a,b]
    a=input('ingrese el intervalo inferior: ');
    b=input('ingrese el intervalo superior: ');
    %esto de aqui grafica la funcion de manera simple
    ezplot(f,[a,b]);
    ylabel('Y');
    xlabel('X');
    grid on 
    hold on
    clc
    fprintf('Metodos iterativos\n');
    fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n'); 
    iteMetho=input('ingrese el metodo iterativo que desea: ');
        switch (iteMetho)
          case 1
            %metodo biseccion:
            clc
            fprintf('metodo biseccion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            while (Ea>Es)
                xr=((x1+x2)/2);
                %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fxr=subs(f,xr);
                f1xr=(fx1*fxr);
                if (f1xr>0)
                    x1=xr;
                else 
                    x2=xr;
                end
                if (i==1)
                    xar=xr;
                    i=i+1;
                else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                end
            end

            fprintf('el resultado es:\n');
            disp(xr);
            fprintf('el error aproximado es: \n');
            disp(Ea);
          case 2
            %metodo falsa posicion:
            clc
            fprintf('metodo falsa posicion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));

            fx1=subs(f,x1);
            fx2=subs(f,x2);
            xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
            fxr=subs(f,xr);

            if (fx1*fxr<0)
                x2 = xr;
            else
                if (fx1*fxr>0)
                    x1 = xr;
                else
                    Ea=0;
                end
            end
            Ea=100000;

            while (Ea>Es)
                xant=xr;
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end

                Ea=abs((xr-xant)/xr)*100;
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
          case 3
            %punto fijo
            clc
            fprintf('metodo punto fijo\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se pide el valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
            end

            %se pide la funcion g(x) 
            try
            g=input('ingrese la funcion g(x)= ');
            catch warning
            fprintf('');
            end

            dg=diff(g,x);

            Es=(0.5*10^(2-cifras));

            if (abs(subs(dg,x0))>1) 
                fprintf('No hay convergencia\n');
                Ea=0;
                xr=0;
            else
                xr=subs(g,x0);
                Ea = abs((xr-x0)/xr)*100;
            end

            while Ea>Es
                x0 = xr;
                xr=subs(g,x0);
                Ea = vpa(abs((xr-x0)/xr)*100);
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 4
            %Newton Raphson
            clc
            fprintf('metodo newton raphson\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');

            while (x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              xi=(x0-(fx0)/(fx0d));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 5
            %Newton Raphson mejorado
            clc
            fprintf('metodo newton mejorado\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se pide valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while(x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            calculo=(0.5*10^(2-cifras));
            Es=calculo;
            Ea=10000000000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              fx0d2=subs(deriDos,x0);
              xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 6
            %secante
            clc
            fprintf('metodo secante\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
             x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end
            Es=(0.5*10^(2-cifras));
            Ea=1000000;
            i=1;
            while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx1=subs(f,x1);
              fx2=subs(f,x2);
              xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
              x1=x2;
              x2=xi;
              if (i==1)
                xar=xi;
                i=i+1;
              else 
                Ea=(abs((xi-xar)/xi)*100);
                xar=xi;
                i=i+1;
              end
            end

            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
        end

elseif (tipof==5)
    clc
    try
    f=input('ingrese la funcion exponencial. Ejem( exp(x)+2*x ); f(x)= ');
    catch warning
    fprintf('');
    end
    %aqui se pide el intervalo [a,b]
    a=input('ingrese el intervalo inferior: ');
    b=input('ingrese el intervalo superior: ');
    %esto de aqui grafica la funcion de manera simple
    ezplot(f,[a,b]);
    ylabel('Y');
    xlabel('X');
    grid on 
    hold on
    clc
    fprintf('Metodos iterativos\n');
    fprintf('Los metodos para esta funcion son:\n1.Biseccion\n2.Falsa Posicion\n3.Punto Fijo\n4.Newton Raphson\n5.Newton Raphson Mejorado\n6.Secante\n'); 
    iteMetho=input('ingrese el metodo iterativo que desea: ');
        switch (iteMetho)
          case 1
            %metodo biseccion:
            clc
            fprintf('metodo biseccion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            while (Ea>Es)
                xr=((x1+x2)/2);
                %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                fxr=subs(f,xr);
                f1xr=(fx1*fxr);
                if (f1xr>0)
                    x1=xr;
                else 
                    x2=xr;
                end
                if (i==1)
                    xar=xr;
                    i=i+1;
                else 
                    Ea=(abs((xr-xar)/xr)*100);
                    xar=xr;
                    i=i+1;
                end
            end

            fprintf('el resultado es:\n');
            disp(xr);
            fprintf('el error aproximado es: \n');
            disp(Ea);
          case 2
            %metodo falsa posicion:
            clc
            fprintf('metodo falsa posicion\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end

            Es=(0.5*10^(2-cifras));

            fx1=subs(f,x1);
            fx2=subs(f,x2);
            xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
            fxr=subs(f,xr);

            if (fx1*fxr<0)
                x2 = xr;
            else
                if (fx1*fxr>0)
                    x1 = xr;
                else
                    Ea=0;
                end
            end
            Ea=100000;

            while (Ea>Es)
                xant=xr;
                fx1=subs(f,x1);
                fx2=subs(f,x2);
                xr=vpa(x2-(fx2*(x1-x2))/(fx1-fx2));
                fxr=subs(f,xr);

                if (fx1*fxr<0)
                    x2 = xr;
                else
                    if (fx1*fxr>0)
                        x1 = xr;
                    else
                        Ea=0;
                    end
                end

                Ea=abs((xr-xant)/xr)*100;
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
          case 3
            %punto fijo
            clc
            fprintf('metodo punto fijo\n');
            cifras=input('ingrese el numero de cifras significativas: ');
                
            %se pide el valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while (x0<a || x0>b)
                x0=input('ingrese el valor de x0: ');
            end

            %se pide la funcion g(x) 
            try
            g=input('ingrese la funcion g(x)= ');
            catch warning
            fprintf('');
            end

            dg=diff(g,x);

            Es=(0.5*10^(2-cifras));

            if (abs(subs(dg,x0))>1) 
                fprintf('No hay convergencia\n');
                Ea=0;
                xr=0;
            else
                xr=subs(g,x0);
                Ea = abs((xr-x0)/xr)*100;
            end

            while Ea>Es
                x0 = xr;
                xr=subs(g,x0);
                Ea = vpa(abs((xr-x0)/xr)*100);
            end

            fprintf('el resultado es:\n');
            disp(vpa(xr));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 4
            %Newton Raphson
            clc
            fprintf('metodo newton raphson\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');

            while (x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            Es=(0.5*10^(2-cifras));
            Ea=100000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              xi=(x0-(fx0)/(fx0d));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 5
            %Newton Raphson mejorado
            clc
            fprintf('metodo newton mejorado\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se pide valor de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
            x0=input('ingrese el valor de x0: ');
            while(x0<a || x0>b)
            x0=input('ingrese el valor de x0: ');
            end
            %verfica la convergencia
            deriUno=diff(f,x);
            deriDos=diff(deriUno,x);
            fcero=subs(f,x0);
            fderiu=subs(deriUno,x0);
            fderid=subs(deriDos,x0);
            convergencia=(abs((fcero*fderid)/(fderiu^2)));
            calculo=(0.5*10^(2-cifras));
            Es=calculo;
            Ea=10000000000;
            i=1;
            if (convergencia<1)
              while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx0=subs(f,x0);
              fx0d=subs(deriUno,x0);
              fx0d2=subs(deriDos,x0);
              xi=(x0-(fx0*fx0d)/(fx0d^2-(fx0*fx0d2)));
              Ea=(abs((xi-x0)/xi)*100);
              x0=xi;
              end
            else 
              fprintf('no existe convergencia\n');
              xi=0;
              Ea=0;
            end
            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error es: \n');
            disp(vpa(Ea));
            
          case 6
            %secante
            clc
            fprintf('metodo secante\n');
            cifras=input('ingrese el numero de cifras significativas: ');
            
            %se piden los 2 valores de inicio para el metodo 
            fprintf('en base a la grafica ingrese los siguientes datos:\n');
             x1=input('ingrese el valor de x1: ');
            while (x1<a || x1>b)
                x1=input('ingrese el valor de x1: ');
            end

            x2=input('ingrese el valor de x2: ');
            while ((x1<a || x1>b) || x2<x1)
                x2=input('ingrese el valor de x2: ');
            end
            Es=(0.5*10^(2-cifras));
            Ea=1000000;
            i=1;
            while (Ea>Es)

              %el atributo subs(f,x1) calcula la funcion con x= x1....si x1=2 calcula la funcion digamos f(X)=x^2 en 2..lo que daria 4.
              fx1=subs(f,x1);
              fx2=subs(f,x2);
              xi=(x2-((fx2*(x1-x2))/(fx1-fx2)));
              x1=x2;
              x2=xi;
              if (i==1)
                xar=xi;
                i=i+1;
              else 
                Ea=(abs((xi-xar)/xi)*100);
                xar=xi;
                i=i+1;
              end
            end

            fprintf('el resultado es:\n');
            disp(vpa(xi));
            fprintf('el error aproximado es: \n');
            disp(vpa(Ea));
            
        end        
    
    end
    clear
    continuar=input('Desea continuar?\n1.SI \nOtro numero NO\n');
    if continuar ==1
        tipof=1;
    else
        tipof=6;
        fprintf('Integrantes:\nVladimir Enrique Martinez Flores MF18030\nMelvin Ernesto Portillo Merlos PM18008\n')
    end
end 
            



