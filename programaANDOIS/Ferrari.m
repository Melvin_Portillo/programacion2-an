%metodo de la ferrari programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
%calculos pertinentes...xd
valoresNum=coeffs(f,'all');
w=numel(valoresNum);
grado=w-1;
s=valoresNum(1,1);
if (s==1)
  %caso ferrari
  a=valoresNum(1,2);
  b=valoresNum(1,3);
  c=valoresNum(1,4);
  d=valoresNum(1,5);
else
  a=((valoresNum(1,2))/s);
  b=((valoresNum(1,3))/s);
  c=((valoresNum(1,4))/s);
  d=((valoresNum(1,5))/s);
end
K=(-a/4);
P=((8*b-3*a^2)/8);
Q=((8*c-4*a*b+a^3)/8);
R=((256*d-64*a*c+16*a^2*b-3*a^4)/256);

ap=(-P/2);
bp=(-R);
cp=((4*P*R-Q^2)/8);

Pp=(bp-((ap^2)/3));
Qp=(cp-((ap*bp)/3)+((2*(ap^3))/27));

E=(((4*Pp^3+27*Qp^2)/108));
kp=(-ap/3);

if (E<0)
  fi=acos((sqrt(27)*Qp)/(2*Pp*sqrt((-Pp))));
  U=((2*sqrt(-Pp/3)*cos(fi/3))+kp);
	V=(sqrt(2*U-P));
  W=((Q)/(-2*V));
  x1=(((V+sqrt(V^2-4*(U-W)))/(2))+K);
	x2=(((V-sqrt(V^2-4*(U-W)))/(2))+K);
	x3=(((-V+sqrt(V^2-4*(U+W)))/(2))+K);
	x4=(((-V-sqrt(V^2-4*(U+W)))/(2))+K);
  fprintf('Las raices son:\n');
  fprintf('x= ');
  disp(vpa(x1));
  fprintf('\nx= ');
  disp(vpa(x2));
  fprintf('\nx= ');
  disp(vpa(x3));
  fprintf('\nx= ');
  disp(vpa(x4));
else 
  D=sqrt(E);
  if (D>0)
    A=vpa(double(-Qp/2+D))
    B=vpa(double(-Qp/2-D))
    
    if (A<0 & B<0)
      Ag=double(-A);
      Bg=double(-B);
      Fg=nthroot(Ag,3)+nthroot(Bg,3);
      U=vpa((-1*Fg)+kp)
      V=vpa((sqrt(2*U-P)))
      W=vpa(((Q)/(-2*V)))
    elseif (A<0)
      Ag=-A;
      Fg=nthroot(Ag,3);
      fd=(-1*Fg+nthroot(B,3));
      U=vpa((fd)+kp)
      V=vpa((sqrt(2*U-P)))
      W=vpa(((Q)/(-2*V)))
    elseif (B<0)
      Bg=-B;
      Fg=nthroot(Bg,3);
      fd=(-1*Fg+nthroot(A,3));
      U=vpa((fd)+kp)
      V=vpa((sqrt(2*U-P)))
      W=vpa(((Q)/(-2*V)))
      
    else 
      U=vpa(((nthroot(A,3)+nthroot(B,3))+kp))
      V=vpa((sqrt(2*U-P)))
      W=vpa(((Q)/(-2*V)))
      
    end
      x1=(((V+sqrt(V^2-4*(U-W)))/(2))+K);
	    x2=(((V-sqrt(V^2-4*(U-W)))/(2))+K);
	    x3=(((-V+sqrt(V^2-4*(U+W)))/(2))+K);
  	  x4=(((-V-sqrt(V^2-4*(U+W)))/(2))+K);
      fprintf('Las raices son:\n');
      fprintf('x= ');
      disp(vpa(x1));
      fprintf('\nx= ');
      disp(vpa(x2));
      fprintf('\nx= ');
      disp(vpa(x3));
      fprintf('\nx= ');
      disp(vpa(x4));
    
  
  else 
    if (Qp>0)
      xx=double((Qp)/2);
      xs=nthroot(double(xx),3);
      xd=-xs;
      U=(2*xd+kp);
      
    else 
      xx=((-Q)/2);
      xs=nthroot(double(xx),3);
      U=(2*xs+kp);
      
    end
    
    V=(sqrt(2*U-P));
    W=((Q)/(-2*V));
    x1=(((V+sqrt(V^2-4*(U-W)))/(2))+K);
	  x2=(((V-sqrt(V^2-4*(U-W)))/(2))+K);
	  x3=(((-V+sqrt(V^2-4*(U+W)))/(2))+K);
  	x4=(((-V-sqrt(V^2-4*(U+W)))/(2))+K);
    fprintf('Las raices son:\n');
    fprintf('x= ');
    disp(vpa(x1));
    fprintf('\nx= ');
    disp(vpa(x2));
    fprintf('\nx= ');
    disp(vpa(x3));
    fprintf('\nx= ');
    disp(vpa(x4));
    
  end
end
