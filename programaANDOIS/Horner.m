%metodo de Horner programa2AN

%esto convierte a x en una variable simbolica
syms x
%aqui se pide la funcion pero esta debera ser borrada, puesto que se pedira antes y se le dara a conocer los metodos que esa funcion tiene para sacar las raices
try
f=input('ingrese la funcion f(x)= ');
catch warning
fprintf('');
end

cifras=input('ingrese el numero de cifras significativas: ');
%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');
b=input('ingrese el intervalo superior: ');
%esto de aqui grafica la funcion de manera simple
ezplot(f,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
%se piden los 2 valores de inicio para el metodo 
fprintf('en base a la grafica ingrese los siguientes datos:\n');
x0=input('ingrese el valor de x0: ');
while (x0<a || x0>b)
    x0=input('ingrese el valor de x0: ');
end

Es=(0.5*10^(2-cifras));

A=coeffs(f,'all');
grado=numel(A)-1;

for i=1:1:grado+1
    if(i==1)
       B(1)=A(1);
    else
        B(i)=A(i)+B(i-1)*x0;
    end
end

R0=B(grado+1);

for i=1:1:grado
    if(i==1)
       C(1)=B(1);
    else
        C(i)=B(i)+C(i-1)*x0;
    end
end

S1=C(grado);


xr=vpa(x0-(R0/S1));
Ea=vpa(abs((xr-x0)/xr)*100);

while(Ea>Es)
    x0=xr;
    
    for i=1:1:grado+1
        if(i==1)
           B(1)=A(1);
        else
            B(i)=A(i)+B(i-1)*x0;
        end
    end

    R0=B(grado+1);

    for i=1:1:grado
        if(i==1)
           C(1)=B(1);
        else
            C(i)=B(i)+C(i-1)*x0;
        end
    end

    S1=C(grado);


    xr=vpa(x0-(R0/S1));
    Ea=vpa(abs((xr-x0)/xr)*100);
    
end

fprintf('el resultado es:\n');
disp(vpa(xr));
fprintf('el error aproximado es: \n');
disp(vpa(Ea));

